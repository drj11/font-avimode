# Avimode

An original design and a work-in-progress.
It currently has Latin (Capitals only, by design) with a
reasonable but not extensive range of diacritics.

Inspired by a fusion of ideas:

-   counters should have counters inside them;
-   circuit-board traces;
-   Fill The Space.

The initial alphabet was developed from a rough paper sketch of
a couple of letters, which was developed into the basic alphabet
over a couple of days using digital methods.

It is currently undergoing a period of commercial evaluation.
It is awaiting expansion to full latin.


## Names

The name has changed from Acacounter
(because the counters have counters, so there is a repeat)
to Avi (anagram of via, a through-way on a PCB) to _Avimode_.

Names considered:

- Avi
- Acacounter (working title for a while)
- Avicounter
- Aviform
- Avilicious
- Avimorph
- Avimol
- Avimode


## snags

The % is awful.


# END
