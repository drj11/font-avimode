# Avimode

Avimode is bold, sharp, and futuristic.
An original CubicType design with a design inspired by
the holes and tracks on printed circuit boards.
Text set in Avimode fills almost all the space available to it,
and has small details.
CubicType therefore recommends using this type at large sizes
and with processes that are faithful to its fine details.
It would look great cut 2 metres high on the side of your
galactic spaceship.
Please be aware that some of the lettershapes have sharp pointy
corners: HANDLE WITH CARE!


## keywords

space
future
sharp
extra bold
scifi
geometric
techno
display
tiny circular counters
packaging
titles

# END
